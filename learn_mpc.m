%% learn_mpc.m
% *Summary:* Script to run a model predictive control process with
% iterative retraining of the dynamics model
%
% Last modified by HAMF: 2022-03-06
%
%% High-Level Steps
% # Initialization
% # Run MPC predict-implement loop


%% Code

% 1. Initialisation
if exist('mpc_preinit','var')
    
    
else
    clear all; close all;
    settings_vehicle
    load('data\targets\moose_target_2.mat')
    use_existing_dynmodel = true;
    TMAX = 163; H = 20; STEP = 10; ITER = 1;
    dynmodel.induce = zeros(140,0,1);
    t = linspace(0, TMAX*dt-dt, TMAX+1);
    delta_i = zeros(1,TMAX);
    for i = 1:length(delta_i)
        delta_i(i) = 2*(target(2,i+1)-target(2,i));
    end
    z0 = [0 0 0 0]'; ngp = 4;
    s0 = [0.1 0.1 0.1 0.1]'.^3;
end

results = cell(ITER,1);

if use_existing_dynmodel
    load('data\dynmodels\dynmodel_lin_hf.mat')
    ITER = 1;
else
    [x, y] = random_rollout([0 0 0 0]',150,plant);
    dynmodel = trainDynModel(dynmodel,x,y);
end

for iter = 1:ITER
    % 2. Run MPC control over target path
    [z, delta_f, n_pred, zGP, sGP, iGP, delta_pred, cost_evals] = mpc(dynmodel, ...
        plant, cost, delta_i, z0, s0, target, TMAX, H, STEP);
    
    % 3. Add new states to training data
    x = [x; z(:,1:iGP(1,1,end)+STEP-1)' delta_f(1,1:iGP(1,1,end)+STEP-1)'];
    y = [y; (z(:,2:iGP(1,1,end)+STEP) + chol(plant.noise)*randn(ngp,1))'];
    
    % 4. Plot result
    plot(t, target(1,:), 'black')
    hold on
    plot(t(1:length(z)), z(3,:))
%     for p = 1:n_pred
%         plot(t(iGP(1,:,p)), zGP(3,:,p), 'b:')
%         plot(t(iGP(1,1:STEP+1,p)), z(3,iGP(1,1:STEP+1,p)), 'r--')
%     end
    
    % 5 Retrain dynamics model
    if ITER > 1
        dynmodel = trainDynModel(dynmodel,x,y);
    end
    
    result.z = z; result.delta_f = delta_f; result.n_pred = n_pred;
    result.zGP = zGP; result.sGP = sGP; result.iGP = iGP;
    result.delta_pred = delta_pred; result.cost_evals = cost_evals;
    result.dynmodel = dynmodel;
    results{iter} = result;
end


save(['data/results/mpc_results_' datestr(now,'yymmdd_HHMM')], 'results', ...
    't', 'STEP', 'TMAX', 'H', 'plant', 'cost', 'target', 'delta_i', 'z0', ...
    's0', 'ITER')