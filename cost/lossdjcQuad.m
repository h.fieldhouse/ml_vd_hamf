%% lossdjcQuad.m
% *Summary:* Compute expectation and variance of a quadratic cost 
% $(x-z)'*W*(x-z)$
% and their derivatives, where $x \sim N(m,S)$
%
%
%  function [L, dLdm, dLds] = lossdjcQuad(cost, z, m, s)
%
%
%
% *Input arguments:*
%
%    cost
%      .W      weight matrix                                            [D x D]
%      .b      weighting on variance  
%    z         target state                                             [D x 1]
%    m         mean of input distribution                               [D x 1]
%    s         covariance matrix of input distribution                  [D x D]
%
%
% *Output arguments:*
%
%   L               expected loss                                  [1   x    1 ]
%   dLdm            derivative of L wrt input mean                 [1   x    D ]
%   dLds            derivative of L wrt input covariance           [1   x   D^2] 
%   S               variance of loss                               [1   x    1 ]
%   dSdm            derivative of S wrt input mean                 [1   x    D ]
%   dSds            derivative of S wrt input covariance           [1   x   D^2]    
%   C               inv(S) times input-output covariance           [D   x    1 ]   
%   dCdm            derivative of C wrt input mean                 [D   x    D ]  
%   dCds            derivative of C wrt input covariance           [D   x   D^2]  
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified by DJC:  2021-03-21
%
function [L, dLdm, dLds] = lossdjcQuad(cost, z, m, s)

b = cost.b; W = cost.W;

% 1. expected cost
L = b*s(:)'*W(:) + (z-m)'*W*(z-m); % b is weighting on variance s, default is b=1. %  

% 1a. derivatives of expected cost
dLdm = 2*(m-z)'*W; % wrt input mean
dLds = b*W';         % wrt input covariance matrix

