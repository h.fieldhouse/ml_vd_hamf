function [J, dJdU]=cost_path_dynamics(delta,plant,z0,~,target,cost)
% Cost function to optmise path using only true vehicle dynamics


%% Code

% 1. Initialisation
H = length(delta);

% 2. Predict sequence
zDyn = zeros(4,H+1); zDyn(:,1) = z0; 

OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times

for i = 1:H                        % simulate in loop
   [~,next]=ode45(@(tode,z) plant.dynamics(tode,z,delta(i)),tsim,zDyn(:,i)',OPTIONS);
   zDyn(:,i+1) = next(3,:)';
end

zDyn = zDyn(:,2:end);

target = [zeros(2,H); target; zeros(1,H)];
zDyn = [zDyn; delta] - target;

% 3. Calculate cost
J_M = 0;
for i = 1:H
    J_M = J_M + (zDyn(:,i)' * cost.W * zDyn(:,i));
end

J = J_M;
dJdU = nan; % only for compatability

end