%% loss_vehicle.m
% *Summary:* 
%
%
%   function [J, dJdm, dJds, S2] = cost_path(cost, target, z0, s0, delta)
%
%
% *Input arguments:*
%
%   cost            cost structure
%     .W            state variable weights to cost                  [D x  1 ]
%     .b            variance cost                                   
%   target          target path                                     [2 x  H ]
%   M               initial state                                   [E x  1 ]
%   S               initial variance                                [E x  E ]
%   delta           steer angle delta                               [1 x  H ]
%
% *Output arguments:*
%
%   J     expected cost                                             [1 x  1 ]
%
% Copyright (C) 2008-2013 by 
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified by HAMF: 2022-01-02
%
%% High-Level Steps
% # Precomputations
% # Define static penalty as distance from target setpoint
% # Trigonometric augmentation    
% # Calculate loss

function [J, dJdU] = cost_path_no_diff(delta,dynmodel,z0,S0,target,cost)
%% Code

% 1. Initialisation
H = length(delta);

% 2. Predict sequence
zGP = zeros(4,H+1); sGP = zeros(H+1,25); % ,4);
zGP(:,1) = z0; % sGP(1,:) = s0(1:4);
sgp=[S0 zeros(4,1); zeros(1,5)];
sGP(1,:) = sgp(:)';

for i = 1:H                        % simulate in loop
   [Mgp, Sgp] = dynmodel.fcn(dynmodel, [zGP(:,i); delta(i)], sgp); 

   zGP(:,i+1) = Mgp;
   sgp(1:4,1:4) = Sgp; 
   sGP(i+1,:) = sgp(:)'; % diag(sgp(1:4,1:4));
end

zGP = zGP(:,2:end);
sGP = sGP(2:end,:);

target = [zeros(2,H); target; zeros(1,H)];
zGP = [zGP; delta] - target;

% 3. Calculate cost
J_M = 0;
for i = 1:H
    J_M = J_M + (zGP(:,i)' * cost.W * zGP(:,i));
end
J_S = sum(cost.b * sGP * cost.W(:));

J = J_M + J_S;
dJdU = nan; % only for compatability
