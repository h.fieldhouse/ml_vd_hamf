function [J, dJdU]=cost_path_mean_f(delta,dynmodel,z0,~,target,cost)
% Cost function to pre-optmise path using only mean function

% inputs:
%   uvec        input sequence                                       [ 1 x H ]
%   dynmodel    gp model
%   M           mean of the output distribution  v, omega, y, psi    [ E  x  1 ]
%   S           compatability
%   target      target output [ noutputs x H ]  (see assignment of target state z, line 76, and function lossdjcQuad.m)
%   cost
%      .W       weight matrix                                            [D x D]

% outputs:
%   V           total cost
%   dVdu        derivative of V wrt uvec                                [ 1 x H ]
%   Mvec        array of output means                                   [ E x H ]
%   Svec        compatability
%   Lvec        incremental (stage) cost                                [ 1 x H ]

%% Code
% 1. Initialisation
H = length(delta);

% 2. Predict sequence
zMF = zeros(4,H+1); zMF(:,1) = z0; 
Alin = dynmodel.Alin; Blin = dynmodel.Blin; Bd = dynmodel.Bd;

for i = 1:H                        % simulate in loop
   xmf=dynmodel.mean_func(dynmodel.m_coeff,[zMF(1:2,i); delta(i)]);
   xmf = zMF(1:2,i) + xmf;
   zMF(:,i+1)=Alin*zMF(1:4,i)+Blin*delta(i)+Bd*real(xmf);
end

zMF = zMF(:,2:end);

target = [zeros(2,H); target; zeros(1,H)];
zMF = [zMF; delta] - target;

% 3. Calculate cost
J_M = 0;
for i = 1:H
    J_M = J_M + (zMF(:,i)' * cost.W * zMF(:,i));
end

J = J_M;
dJdU = nan; % only for compatability


end
