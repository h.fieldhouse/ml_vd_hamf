function [V,dVdu,Mvec,Svec,Lvec]=cost_path(uvec,dynmodel,M,S,target,cost)
% increments dynmodel up to the prediction horizon H using the control input sequence uvec
% calls gp1d_lin_hf.m and lossdjcQuad.m
% returns the total cost V and derivatives dVdu
% inputs:
%   uvec        input sequence                                       [ 1 x H ]
%   dynmodel    gp model
%   M           mean of the output distribution  v, omega, y, psi    [ E  x  1 ]
%   S           covariance matrix of the output distribution         [ E  x  E ]
%   target      target output [ noutputs x H ]  (see assignment of target state z, line 76, and function lossdjcQuad.m)
%   cost
%      .W       weight matrix                                            [D x D]
%      .b       weighting on variance  
%
% outputs:
%   V           total cost
%   dVdu        derivative of V wrt uvec                                [ 1 x H ]
%   Mvec        array of output means                                   [ E x H ]
%   Svec        array of output covariances                         [ E x E x H ]
%   Lvec        incremental (stage) cost                                [ 1 x H ]


Alin = dynmodel.Alin; Blin = dynmodel.Blin; Bd = dynmodel.Bd;
H = length(uvec); E = length(M); D = E + 1;


V=0; % intialise total cost
dVdu=zeros(1,H); % initialise derivative 1xH

pdLpdmk=zeros(H,4);  % initialise intermediate variables
pdLpdsk=zeros(4,4,H);
dMdmk=zeros(4,4,H);
dMdsk=zeros(4,4,4,H);
dMduk=zeros(4,H);
dSdmk=zeros(4,4,4,H);
dSdsk=zeros(4,4,4,4,H);
dSduk=zeros(4,4,H);

for k=1:H % time step up to the horizon. Calculate total cost V and store derivatives.
    k;
    m=[M; uvec(k)]; % input mean = [preceding values; control input]
    mvec(:,k)=m;
    s=[S zeros(E,1); zeros(1,D)]; % input uvec has zero variance
    svec(:,:,k)=s;

    [M, S]=dynmodel.fcn(dynmodel,m,s); 

    Mvec(:,k)=M;
    Svec(:,:,k)=S;
    % calculate derivatives numerically (time consuming)
    delta=1e-4;
    for i=1:4 % dMdm, dSdm
        mplus=m;
        mplus(i)=mplus(i)+delta;
        [Mplus, Splus]=dynmodel.fcn(dynmodel,mplus,s);
        dMdm(:,i)=(Mplus-M)/delta;
        dSdm(:,:,i)=(Splus-S)/delta;
        for j=1:4 % dMds, dSds
            splus=s;
            splus(i,j)=splus(i,j)+delta;
            [Mplus, Splus]=dynmodel.fcn(dynmodel,m,splus);
            dMds(:,i,j)=(Mplus-M)/delta;
            dSds(:,:,i,j)=(Splus-S)/delta;
        end
    end
    % dMdu, dSdu
    mplus=m;
    mplus(5)=mplus(5)+delta;
    

    [Mplus, Splus]=dynmodel.fcn(dynmodel,mplus,s);
    dMdu=(Mplus-M)/delta;
    dSdu=(Splus-S)/delta;

    
    z=[0; 0; target(1,k); target(2,k); 0];  % actually k+1, but indexed as k in target
    %z=[0; 0; 1; 0; 0]  % simple target for testing
    
% modify inputs to lossdjcQuad to evaluate L at k+1 instead of k        
    m=[M; uvec(k)];              % m at k+1, uvec at k
    s=[S zeros(E,1); zeros(1,D)]; % s at k+1, input uvec has zero variance
    
    [L,pdLpdm,pdLpds]=lossdjcQuad(cost,z,m,s); % incremental (stage) cost and derivative
    Lvec(k)=L; % index k, corresponding to u input, but note L is evaluated for states at k+1
    V=V+L; % add incremental (stage) cost to total cost
    % store the derivatives
    pdLpdmk(k,:)=pdLpdm(1:4);   % note that the numerator of all these derivatives corresponds to k+1
    pdLpdsk(:,:,k)=pdLpds(1:4,1:4);
    dMdmk(:,:,k)=dMdm;
    dMdsk(:,:,:,k)=dMds;
    dMduk(:,k)=dMdu;
    dSdmk(:,:,:,k)=dSdm;
    dSdsk(:,:,:,:,k)=dSds;
    dSduk(:,:,k)=dSdu;
end

pdmkpdu=zeros(E,H); % last two elements are index i in u, index k in prediction horizon
pdskpdu=zeros(E,E,H);

%
% The remainder of the code calculates the derivatives of the total cost
% dVdu. Refer to DJCole for details. This code can be ignored if only V is required.
%

for k=1:H % time step up to the horizon. Calculate derivatives of V.
    % Step 3
    % calculate pdm(k)/pdu
    %           E x H    (:,i,k)
    % calculate pds(k)/pdu
    %           E*E x H   (:,:,i,k)
    % : is elements of m, i is index in u, k is index in prediction horizon
    % pdmkpdu=zeros(E,H,H); % last two elements are index i in u, index k in prediction horizon
    % pdskpdu=zeros(E,E,H,H);
    pdmkpdu=zeros(E,H,H); % last element is index k in horizon
    pdskpdu=zeros(E,E,H,H);
    pdmkpdu(:,k,k)=dMduk(:,k);
    pdskpdu(:,:,k,k)=dSduk(:,:,k);
    
    % Step 2a
    % calculate dm(k)/du = pdm(k)/pdm(k-1) * dm(k-1)/du + pdm(k)/pds(k-1) * ds(k-1)/du + pdm(k)/pdu
    %           ExH        ExE               ExH          ExE*E             E*ExH        ExH
    %                      gp1d              recursive    gp1d              recursive    step 3  
    if k==1 
        dmkdu(:,:,k) = pdmkpdu(:,:,k);
    else
        % pdmkpdsk1*dsk1du reshape and multiply
        tempa=reshape(dMdsk(:,:,:,k),[4 4*4]);
        tempb=reshape(pdskpdu(:,:,:,k-1),[4*4 H]);
        dmkdu(:,:,k) = dMdmk(:,:,k)*dmkdu(:,:,k-1) + reshape(tempa*tempb,[4 H]) + pdmkpdu(:,:,k);
    end
    % Step 2b
    % calculate ds(k)/du = pds(k)/pdm(k-1) * dm(k-1)/du + pds(k)/pds(k-1) * ds(k-1)/du + pds(k)/pdu
    %           E*ExH      E*ExE             ExH          E*ExE*E           E*ExH        E*ExH
    %                      gp1d(k-1)         recursive    gp1d(k-1)         recursive    step 3  
    
    if k==1
        dskdu(:,:,:,k)=pdskpdu(:,:,:,k);
    else
        % pdskpdmk1*dmk1du reshape and multiply
        tempc=reshape(dSdmk(:,:,:,k-1),[4*4 4]);
   
        % pdskpdsk1*dsk1du reshape and multiply
        tempa=reshape(dSdsk(:,:,:,:,k-1),[4*4 4*4]);
        tempb=reshape(dskdu(:,:,:,k-1),[4*4 H]);
    
        dskdu(:,:,:,k) = reshape(tempc*dmkdu(:,:,k-1),[4,4,H]) + reshape(tempa*tempb,[4,4,H]) + pdskpdu(:,:,:,k);
    end
    
    % Step 1a
    % pdL(k)/pdm(k): pdLpdm   1xE
    % Step 1b
    % pdL(k)/pds(k): pdLpds   1xE
    %
    % Step 0
    % calculate dL(k)/du = pdL(k)/pdm(k) * dm(k)/du + pdL(k)/pds(k) * ds(k)/du
    %           1xH        1xE             ExH        1xE*E           E*ExH
    %                      lossdjcQuad     step 2     lossdjcQuad     step 2
    
    % pdLpds*dskdu reshape and multiply
    tempa=reshape(pdLpdsk(:,:,k),[1 4*4]);
    tempb=reshape(dskdu(:,:,:,k),[4*4 H]);
    dLdu = pdLpdmk(k,:)*dmkdu(:,:,k) + reshape(tempa*tempb,[1 H]);
    dVdu = dVdu + dLdu; % add incremental gradient to total gradient
    
end