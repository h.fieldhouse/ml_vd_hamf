%% minimize_cost.m
% *Summary:* Script to minimize path cost by selecting optimal inputs
%
% 
% 
% *Example Usage:*
%
% settings_vehicle
% load('C:\Users\harry\Documents\MATLAB\pilcoV0.9\scenarios\hamf-vehicle-v2\data\dynmodel_lin_hf.mat')
% H = 30;
% t = linspace(0, H*dt-dt, H)';
% delta = 0.125*sin(t*2*pi/(H*dt));
% z0 = [0 0 0 0];
% s0 = [0.1 0.1 0.1 0.1].^3;
% target = zeros(30,2);
% minimize_cost;
% 
% Last modified by HAMF: 2022-01-02
%
%% High-Level Steps

%% Code

options = optimoptions('fmincon','Display','iter','Algorithm','interior-point');
problem.options = options;
problem.solver = 'fmincon';
problem.objective = @(delta)f(dynmodel, delta, z0, s0, cost, target);
problem.x0 = zeros(30,1);
problem.ub = ones(30,1).*policy.maxU;
problem.lb = - problem.ub;

[x,fval] = fmincon(problem);

function J = f(dynmodel, delta, z0, s0, cost, target)
[M,S] = predict_path(dynmodel, delta, z0, s0);
[J] = cost_path(cost, target, M, S, delta);
end