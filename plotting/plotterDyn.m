%% plotterDyn.m
% *Summary:* Script to run a dynamics simulation
%
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-30
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

%% Code

% 1. Initialization
%settings_vehicle;                      % load scenario-specific settings
%basename = 'vehicle_';                 % filename used for saving data

z0 = [0 0];
maxU = 0.5;
maxOm = 2;
N = 20;
z = zeros(N*N,2); s = zeros(N*N,4);

plot_omega = true;

if plot_omega
    delta = linspace (-maxU,maxU,N); omega = linspace (-maxOm,maxOm,N);
    [Delta, Omega] = meshgrid(delta, omega);
    Delta = reshape(Delta,1,[]); Omega = reshape(Omega,1,[]);
    V = zeros(1,N*N);
    s_ind = 2;
else
    delta = linspace (-maxU,maxU,N); v = linspace (-maxV,maxV,N);
    [Delta, V] = meshgrid(delta, v);
    Delta = reshape(Delta,1,[]); V = reshape(V,1,[]);
    Omega = zeros(1,N*N);
    s_ind = 1;
end

OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times

for i = 1:length(Delta)
    [tode,next]=ode45(@(tode,z) plant.dynamics(tode,z,Delta(i)),tsim,[V(i) Omega(i) 0 0]);
    z(i,:)=(next(3,1:2)); %+ (chol(plant.noise(1:2,1:2))*randn(ngp,1))');
end

% Figure = figure(1); clf(1);
% ax1 = nexttile;
% plot(ax1,delta,z(:,2),'r','linewidth',2)
% plot(ax1,delta,z(:,2)+sqrt(s(:,2)),'r','linewidth',0.5)
% plot(ax1,delta,z(:,2)-sqrt(s(:,2)),'r','linewidth',0.5)
% ylabel(ax1,'omega (rad/s)')
% 
% ax2 = nexttile;
% plot(ax2,delta,z(:,1))
% ylabel(ax2,'Lateral velocity (m/s)')
% xlabel(ax2,'Steering Angle (radians)')

z2 = reshape(z,N,N,[]);

if ~ishandle(2); figure(2); else set(0,'CurrentFigure',2); end; clf(2);
ax1 = axes;
if plot_omega
    surf(ax1,reshape(Delta,N,N),reshape(Omega,N,N),z2(:,:,s_ind));
    cb1 = colorbar(ax1);
    
    xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
    title(ax1,['Plot of Dynamics output omega[k+1] to inputs at [k]: v[k] = ' num2str(V(1))])
else
    surf(ax1,reshape(Delta,N,N),reshape(V,N,N),z2(:,:,s_ind));
    cb1 = colorbar(ax1);
    
    xlabel(ax1,'delta (rad)'); ylabel(ax1,'v[k] (rad/s)'); zlabel(ax1,'v[k+1] (rad/s)')
    title(ax1,['Plot of Dynamics output v[k+1] to inputs at [k]: omega = ' num2str(Omega(1))])
end