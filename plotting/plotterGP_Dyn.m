%% plotterGP_Dyn.m
% *Summary:* Script to run a Gaussian Process dynamics simulation and plot
% against true dynamics model
%
%
%
% Last modified by HAMF: 2021-10-30
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

%% Code

% 1. Initialization
plant.dynamics = @dynamics_nlcar;      % load scenario-specific settings

%load('data/dynmodel_SPGP_100of1800nl_sin_rand_in_2110302238.mat')

z0 = [0 0];
s0 = diag([0.1 0.1 0]);
maxU = 1;
maxOm = 2;
N = 20;
zGP = zeros(N*N,2); s = zeros(N*N,2);
zDyn = zeros(N*N,2);

delta = linspace (-maxU,maxU,N); omega = linspace (-maxOm,maxOm,N);
[Delta, Omega] = meshgrid(delta, omega);
Delta = reshape(Delta,1,[]); Omega = reshape(Omega,1,[]);

for i = 1:length(Delta)
    [M, S] = dynmodel.fcn(dynmodel, [z0(1) Omega(i) Delta(i)]', s0);
    zGP(i,:) = M' + [z0(1),Omega(i)]; s(i,:) = diag(S);
end

tsim = [0 dt/2 dt];

for i=1:length(Delta) % run the plant with uvec and store response
    [tode,next]=ode45(@(tode,z) plant.dynamics(tode,z,Delta(i)),tsim,[z0(1) Omega(i)]);
    zDyn(i,:)=next(3,:)';  % ode45 returns output at all times in tsim.
end

zGP2 = reshape(zGP,N,N,[]);
zDyn2 = reshape(zDyn,N,N,[]);

if ~ishandle(2); figure(2); else; set(0,'CurrentFigure',2); end; clf(2);
ax1 = axes;
mesh(ax1,reshape(Delta,N,N),reshape(Omega,N,N),zDyn2(:,:,2),'facecolor','none');
axis equal
ax2 = axes;
% mesh(ax2,reshape(Delta,N,N),reshape(Omega,N,N),zGP2(:,:,2),'facecolor','none');
axis equal

% Link two axes together
hLink = linkprop([ax1,ax2],{'XLim','YLim','ZLim','CameraUpVector','CameraPosition','CameraTarget','DataAspectRatio'});
set(ax1,'DataAspectRatio',[1 2 2])
view(ax1,-40,20)

% Hide the top axes
ax2.Visible = 'off';
ax2.XTick = [];
ax2.YTick = [];
% Give each one its colormap
colormap(ax1,'autumn')
colormap(ax2,'winter')

% cb2 = colorbar(ax2,'Position',[0.1 0.1 0.05 0.815]); % four-elements vector to specify Position [left bottom width height]
% cb1 = colorbar(ax1,'Position',[0.81 0.1 0.05 0.815]);
% 
% cb1.FontSize = 12;
% cb2.FontSize = 12;
% 
% cb1.Label.String = 'Dynamics Model';
% cb2.Label.String = 'Gaussian Process';
% cb1.Label.FontSize = 18;
% cb2.Label.FontSize = 18;

xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
title(ax1,'Mesh plot of Dynamics model','fontsize',14)

% ['Mesh plot of dynamics and GP models output omega[k+1] to inputs at [k]: [' num2str(z0(1)) ' omega ' ...
%     ' delta]']