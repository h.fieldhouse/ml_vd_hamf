load('model_testing\sparsity_eval_timings_v1.mat')

clf()
ax1 = axes();

loglog(ax1,sparsity_timings(:,1),sparsity_timings(:,2)/N)

xlabel('GP Sparsity', 'fontsize', 14)
ylabel('Computation time per evaluation /sec','fontsize',14)

% title('Logarithmic plot of computation time against sparsity','fontsize',14)

grid on
