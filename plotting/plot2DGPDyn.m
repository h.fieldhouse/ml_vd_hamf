%% plot2DGPDyn.m
% *Summary:* Script to run a Gaussian Process dynamics simulation and plot
% against true dynamics model on a provided axes with provided fixed
% variable values
%
%   function plot2DGPDyn(app)
%
% *Input arguments:*
%
%   app
%   mode        'var' or 'plot'
%
%
% Last modified by HAMF: 2021-10-30
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

function plot2DGPDyn(axes, varinSelect, varoutSelect, plotFixVals, inputRanges, useCovFunc)
%% Code

% Initialise
settings_vehicle

basenameGP = 'data/dynmodels/dynmodel_SPGP_';
if strcmp(useCovFunc,'On')
    basenameGP = 'data/covfunc_dynmodel_SPGP_';
end

nInduce = num2str(plotFixVals(4));
fileID = [nInduce 'of3000ideal_gauss_multivar_2111021009'];

load([basenameGP fileID '.mat'],'dynmodel')


    
n = 100;
var_i = find(varinSelect==0);
not_var_i = find(varinSelect==1);
var_i_str = {'Lateral vel. [k]', 'Omega [k]', 'Delta [k]'};
var_o = find(varoutSelect==1);
var_o_str = {'Lateral vel. [k+1]', 'Omega [k+1]'};
var_o_str = var_o_str{var_o};
inputs = zeros(n,3);
inputs(:,varinSelect)=ones(n,2).*plotFixVals(varinSelect);
inputs(:,var_i) = linspace(inputRanges(var_i,1),inputRanges(var_i,2),n);

outDyn = zeros(n,2);
outGPM = zeros(n,2);
outGPS = zeros(n,2);
s = diag([0 0 0]);

OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times

for i = 1:n                          % simulate in loop
   [M, S] = dynmodel.fcn(dynmodel, [inputs(i,1:2) inputs(i,3)]', s);
   outGPM(i,:) = M' + inputs(i,1:2);
   outGPS(i,:) = diag(S);
   [~,next]=ode45(@(tode,z) plant.dynamics(tode,z,inputs(i,3)),tsim,[inputs(i,1:2) 0 0]',OPTIONS);
   outDyn(i,:) = next(3,1:2);
end

hold(axes,'off')
f = [outGPM(:,var_o)+2*sqrt(outGPS(:,var_o)); flipdim(outGPM(:,var_o)-2*sqrt(outGPS(:,var_o)),1)];
fill(axes, [inputs(:,var_i); flipdim(inputs(:,var_i),1)], f, [7 7 7]/8)
hold(axes,'on'); plot(axes, inputs(:,var_i), outGPM(:,var_o), 'b');
plot(axes, inputs(:,var_i), outDyn(:,var_o), 'r');
legend(axes, '95% Confidence bound', 'Gaussian process','Dynamics model')

title(axes, ['Plot of ' var_o_str ' against ' var_i_str{var_i} '. ' var_i_str{not_var_i(1)} ...
    ' = ' num2str(plotFixVals(not_var_i(1))) ', ' var_i_str{not_var_i(2)} ...
    ' = ' num2str(plotFixVals(not_var_i(2))) ', nInduce = ' nInduce])
end