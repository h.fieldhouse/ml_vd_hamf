result = results{6};
load('data/targets/moose_target_2.mat')
u = 18;

xlimits = [0 8];

tiles = tiledlayout(1,2);

ax1 = nexttile();

plot(t(1:163).*u,result.delta_f)
title('Steering input trace')
xlabel('Forward distance, m')
ylabel('Steering angle, rad')
grid on
%xlim(xlimits)

ax2 = nexttile();

plot(t(1:140).*u, result.z(3,1:140))
hold on
plot(t.*u, target(1,:))
title('Resulting lateral displacement')
xlabel('Forward distance, m')
ylabel('Lateral displacement, m')
grid on
legend('Result','Target')

%xlim(xlimits)