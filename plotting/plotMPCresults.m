% plotMPCresults.m
%
% Script to plot results from one or more iterations of the MPC
%

u = 18;
plotMPC.i = 1:6;

clf()
% plotTargetPos(target, t, u)
hold on

for i = plotMPC.i
    pause
    plotResultZ(results{i},target,t,STEP,3)
%     plotResultPos(results{i},t,u,STEP)
end

function plotResultZ(result, target, t, STEP, state_var)
    % Plot result
    if state_var == 1
        s = 1;tar = 1;
    elseif state_var == 2
        s = 2;tar = 2;
    elseif state_var == 3
        s = 3;tar = 1;
    elseif state_var == 4
        s = 4;tar = 2;
    end
    plot(t, target(tar,:), 'black')
    hold on
    for p = 1:result.n_pred
        plot(t(result.iGP(1,:,p)), result.zGP(s,:,p), 'b:')
        plot(t(result.iGP(1,1:STEP+1,p)), result.z(s,result.iGP(1,1:STEP+1,p)), 'r--')
    end
end

function plotTargetPos(target, t, u)
    pos = zeros(2,length(t));
    for i = 2:length(t)
        pos(:,i) = pos(:,i-1) + u * (t(i)-t(i-1)) * [cos(target(2, i)); sin(target(2, i))];
    end
    plot(pos(1,:), pos(2,:))
end

function plotResultPos(result, t, u, STEP)
    H = size(result.iGP,2);
    posT = zeros(2,length(t));
    posP = zeros(2,STEP,result.n_pred);
    for i = 2:length(t)
        posT(:,i) = posT(:,i-1) + u * (t(i)-t(i-1)) * [cos(result.z(4, i)); sin(result.z(4, i))];
    end
    for p = 1:result.n_pred
        posP(:,1,p) = posT(:,result.iGP(1,1,p));
        for i = 2:H
            posP(:,i,p) = posP(:,i-1,p) + u*(t(i)-t(i-1))*[cos(result.zGP(4,i,p)); sin(result.zGP(4,i,p))];
        end
        plot(posP(1,:,p), posP(2,:,p), 'b:')
    end
    iEND = result.iGP(1,1,end) + STEP;
    plot(posT(1,1:iEND), posT(2,1:iEND))
end