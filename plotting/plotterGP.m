%% plotterGP.m
% *Summary:* Script to run a Gaussian Process dynamics simulation
%
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-19
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

%load('data/dynmodel_SPGP_300of1200rand.mat')

z0 = [0 0];
s0 = diag([0.1 0.1 0.1 0.1 0]);
maxU = 1.5;
maxOm = 2;
N = 20;
z = zeros(N*N,2); s = zeros(N*N,4);

delta = linspace (-maxU,maxU,N); omega = linspace (-maxOm,maxOm,N);
[Delta, Omega] = meshgrid(delta, omega);
Delta = reshape(Delta,1,[]); Omega = reshape(Omega,1,[]);

for i = 1:length(Delta)
    [M, S] = dynmodel.fcn(dynmodel, [z0(1) Omega(i) 0 0 Delta(i)]', s0);
    z(i,:) = M(1:2)'; s(i,:) = diag(S); % + [z0(1),Omega(i)]
end

% Figure = figure(1); clf(1);
% ax1 = nexttile;
% plot(ax1,delta,z(:,2),'r','linewidth',2)
% plot(ax1,delta,z(:,2)+sqrt(s(:,2)),'r','linewidth',0.5)
% plot(ax1,delta,z(:,2)-sqrt(s(:,2)),'r','linewidth',0.5)
% ylabel(ax1,'omega (rad/s)')
% 
% ax2 = nexttile;
% plot(ax2,delta,z(:,1))
% ylabel(ax2,'Lateral velocity (m/s)')
% xlabel(ax2,'Steering Angle (radians)')

z2 = reshape(z,N,N,[]);

if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
ax1 = axes;
scatter3(ax1,dynmodel.inputs(:,3),dynmodel.inputs(:,2),dynmodel.targets(:,2)+dynmodel.inputs(:,2), 60, dynmodel.inputs(:,1),'filled')
axis equal
ax2 = axes;
mesh(ax2,reshape(Delta,N,N),reshape(Omega,N,N),z2(:,:,2),'facecolor','none');
axis equal

% Link two axes together
hLink = linkprop([ax2,ax1],{'XLim','YLim','ZLim','CameraUpVector','CameraPosition','CameraTarget'});
% Hide the top axes
ax2.Visible = 'off';
ax2.XTick = [];
ax2.YTick = [];
% Give each one its colormap
colormap(ax2,'cool')
colormap(ax1,'default')

cb2 = colorbar(ax2,'Position',[0.1 0.1 0.05 0.815]); % four-elements vector to specify Position [left bottom width height]
cb1 = colorbar(ax1,'Position',[0.81 0.1 0.05 0.815]);

cb1.Label.String = 'Original Data (sideslip vel. [k] color coded)';
cb2.Label.String = 'Gaussian Model';
cb1.Label.FontSize = 14;
cb2.Label.FontSize = 14;

xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
title(ax1,['Mesh plot of GP model output omega[k+1] to inputs at [k]: [' num2str(z0(1)) ' omega ' ...
    ' delta] with original data points overlaid'])