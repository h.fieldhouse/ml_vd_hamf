%% plotterMF_GP.m
% *Summary:* Script to run a dynamics simulation
%
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-30
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

%% Code

% 1. Initialization
%settings_vehicle;                      % load scenario-specific settings
%basename = 'vehicle_';                 % filename used for saving data

x = [-1.36 -1.13 25 0.9 -0.73 0 0.33 0.04 0.92 0.01 0.27];
dynmodel.s_coeff = x(1:3);
dynmodel.m_coeff = reshape(x(4:11),4,2);
dynmodel.m_coeff = dynmodel.m_coeff';
mean_func = @(input)meanfunc_sig(dynmodel,input);

z0 = [0 0];
maxU = 0.5;
maxOm = 2;
maxV = 2;
N = 20;
z = zeros(N*N,2); s = zeros(N*N,4);

plot_omega = false;

if plot_omega
    delta = linspace (-maxU,maxU,N); omega = linspace (-maxOm,maxOm,N);
    [Delta, Omega] = meshgrid(delta, omega);
    Delta = reshape(Delta,1,[]); Omega = reshape(Omega,1,[]);
    V = zeros(1,N*N);
    s_ind = 2;
else
    delta = linspace (-maxU,maxU,N); v = linspace (-maxV,maxV,N);
    [Delta, V] = meshgrid(delta, v);
    Delta = reshape(Delta,1,[]); V = reshape(V,1,[]);
    Omega = zeros(1,N*N);
    s_ind = 1;
end

for i = 1:length(Delta)
    z(i,:)=mean_func([V(i) Omega(i) Delta(i)]')';
end

% Figure = figure(1); clf(1);
% ax1 = nexttile;
% plot(ax1,delta,z(:,2),'r','linewidth',2)
% plot(ax1,delta,z(:,2)+sqrt(s(:,2)),'r','linewidth',0.5)
% plot(ax1,delta,z(:,2)-sqrt(s(:,2)),'r','linewidth',0.5)
% ylabel(ax1,'omega (rad/s)')
% 
% ax2 = nexttile;
% plot(ax2,delta,z(:,1))
% ylabel(ax2,'Lateral velocity (m/s)')
% xlabel(ax2,'Steering Angle (radians)')

z2 = reshape(z,N,N,[]);

if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
tiles = tiledlayout(1,2);
ax1 = nexttile();
if plot_omega
    surf(ax1,reshape(Delta,N,N),reshape(Omega,N,N),z2(:,:,s_ind));
    cb1 = colorbar(ax1);
    
    xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
    title(ax1,['Plot of Mean Function output omega[k+1] to inputs at [k]: v = ' num2str(V(1))])
else
    surf(ax1,reshape(Delta,N,N),reshape(V,N,N),z2(:,:,s_ind));
    cb1 = colorbar(ax1);
    
    xlabel(ax1,'delta (rad)'); ylabel(ax1,'v[k] (rad/s)'); zlabel(ax1,'v[k+1] (rad/s)')
    title(ax1,['Plot of Dynamics output v[k+1] to inputs at [k]: omega = ' num2str(Omega(1))])
end

ax2 = nextile();