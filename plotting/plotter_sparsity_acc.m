load('data\model_testing\sparsity_comparison_v2.mat')
load('data\model_testing\sparsity_eval_timings_v2.mat')

nIter = size(sparsity_range,2);
sp_r_str = cell(nIter,1);

for iter = 1:nIter
    sp_r_str{iter} = [num2str(sparsity_range(iter))]; % '\leftarrow' 
end

clf()
ax1 = axes();
hold on
scatter(ax1, (sparsity_timings(:,2)/N), (sqrt(sse/(2*N))),150, 'Marker', 'x')
set(gca, 'xscale', 'log')
grid on

text(1.1*sparsity_timings(:,2)/N,sqrt(sse/(2*N)),sp_r_str, 'FontSize', 12)

xlabel(ax1, 'Computation time per evaluation /seconds', 'FontSize', 14)
ylabel(ax1, 'RMS GP approximation error', 'FontSize', 14)
title(ax1, 'Plot of GP error against evaluation time (Sparsity labelled)', 'FontSize', 18)