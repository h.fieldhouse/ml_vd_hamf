clear all; close all;

settings_vehicle
load('C:\Users\harry\Documents\MATLAB\pilcoV0.9\scenarios\hamf-vehicle-v2\data\dynmodel_lin_hf.mat')

H = 60;

t = linspace(0,H*dt,H);
target_y = 5 * (1 - cos(t*pi/60));
target_psi = atan((sin(t*pi/60)*pi/12));

target = [target_y; target_psi];

delta = zeros(1,H);

z0 = [0 0 0 0]'; s0 = diag([0 0 0 0]);

minimize_cost % Returns to variables x, fval
