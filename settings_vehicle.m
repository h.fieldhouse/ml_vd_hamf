%% settings_vehicle.m
% *Summary:* Script set up the cart-pole scenario
%
% Copyright (C) 2008-2013 by 
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified by HAMF: 2021-10-13
%
%% High-Level Steps
% # Define state and important indices
% # Set up scenario
% # Set up the plant structure
% # Set up the policy structure
% # Set up the cost structure
% # Set up the GP dynamics model structure
% # Parameters for policy optimization
% # Plotting verbosity
% # Some array initializations

%% Code

rand('seed',1); randn('seed',1); format short; format compact; 
% include some paths
try
  rd = '';
  addpath([rd 'cost'],[rd 'util'],[rd 'gp'],[rd 'dynamics'],[rd 'training']);
catch
end


% 1. Define state and important indices

% 1a. Full state representation (including all augmentations)
%
%  1  v          sideslip velocity
%  2  omega      yaw velocity
%  3  y          lateral displacement
%  4  theta      yaw angle
%  5  delta      inout steering angle
%

% 1b. Important indices
% odei  indicies for the ode solver
% augi  indicies for variables augmented to the ode variables
% dyno  indicies for the output from the dynamics model and indicies to loss
% angi  indicies for variables treated as angles (using sin/cos representation)
% dyni  indicies for inputs to the dynamics model
% poli  indicies for the inputs to the policy
% difi  indicies for training targets that are differences (rather than values)

odei = [1 2 3 4];      % varibles for the ode solver
augi = [];             % variables to be augmented
dyno = [1 2];          % variables to be predicted (and known to loss)
angi = [];             % angle variables
dyni = [1 2];          % variables that serve as inputs to the dynamics GP
difi = [1 2];          % variables that are learned via differences


% 2. Set up the scenario
dt = 0.05;                         % [s] sampling time
T = 5.0;                           % [s] initial prediction horizon time
H = ceil(T/dt);                    % prediction steps (optimization horizon)
mu0 = [0 0 0 0]';                  % initial state mean (y=1m)
S0 = diag([1 1 1 1]);              % initial state covariance
N = 100;                           % number controller optimizations
J = 4;                             % initial J trajectories of length H
K = 1;                             % no. of initial states for which we optimize
nc = 10;                           % number of controller basis functions

% Dynamics model
U = 18;                            % forward velocity [m/s]
a = 1.2;                         % [m] CoM to front axle

% 3. Plant structure
plant.dynamics = @dynamics_nlcar;               % dynamics ode function
plant.noise = diag([0.2 0.1 0.01 0.01].^2.*0.01);      % measurement noise
plant.dt = dt;
plant.maxU = 0.5;

% 4. Set up the cost structure
cost.fcn = @cost_path_no_diff;                  % cost function
cost.derivative = false;
cost.mpc_maxfeval = 1000;
cost.mpc_steptol = 1e-7;

%   W       Weight matrix               `       [ D x D ]
% (1,1) sideslip vel v;     (2,2) yaw vel omega;     (3,3) lateral displacement y
% (4,4) heading angle psi;  (5,5) steering angle u
Wstar=zeros(4);
Wstar(1,1)=1;      % penalise y path following error
Wstar(2,2)=15;     % penalise psi path following error
Wstar(3,3)=5;      % penalise steering angle
Wstar(4,4)=50;     % penalise alpha_f front slip angle

C=[ 0 0 1 0 0           % transform state vector to outputs of interest
    0 0 0 1 0
    0 0 0 0 1
    1/U a/U 0 0 -1 ];

cost.W=C'*Wstar*C;
cost.b=1;               % weighting on variance in quadratic cost function, standard is 1. 
                        % For exploration set <1. For caution set >1.
clear Wstar C

% 5. Dynamics model structure
dynmodel.fcn = @gp1d_lin_hf;         % function for GP predictions
dynmodel.mean_func = @meanfunc_sig;  % function for adjusting pre-GP mean
dynmodel.train = @train;             % function to train dynamics model
dynmodel.induce = zeros(10,0,1);    % shared inducing inputs (sparse GP)
                                     % adjust sparsity here
dynmodel.dyni = dyni;
dynmodel.dyno = dyno;
dynmodel.difi = difi;

dynmodel.m_coeff = [ 0.9 -0.73 0, 0.33
                     0.04 0.92 0.01 0.27 ];

dynmodel.s_coeff = [ -1.36 -1.13 25 ];
                                     
dynmodel.Alin=[ 0 0 0 0
                0 0 0 0
                dt 0 1 U*dt
                0 dt 0 1 ];

dynmodel.Blin=[ 0
                0
                0
                0 ];

dynmodel.Bd = [ 1 0
                0 1
                0 0
                0 0 ];
                                     
dynmodel.trainOpt = [300 500];       % defines the max. number of line searches
                                     % when training the GP dynamics models
                                     % trainOpt(1): full GP,
                                     % trainOpt(2): sparse GP (FITC)

% % 7. Parameters for policy optimization
% opt.length = 150;                        % max. number of line searches
% opt.MFEPLS = 30;                         % max. number of function evaluations
%                                          % per line search
% opt.verbosity = 1;                       % verbosity: specifies how much 
%                                          % information is displayed during
%                                          % policy learning. Options: 0-3

% 8. Plotting verbosity
plotting.verbosity = 1;            % 0: no plots
                                   % 1: some plots
                                   % 2: all plots

% 9. Some initializations
x = []; y = [];
% fantasy.mean = cell(1,N); fantasy.std = cell(1,N);
% realCost = cell(1,N); M = cell(N,1); Sigma = cell(N,1);