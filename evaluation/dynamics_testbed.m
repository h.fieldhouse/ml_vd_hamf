%% Code

% 1. Initialization
settings_vehicle;                      % load scenario-specific settings
basename = 'vehicle_';                 % filename used for saving data

z0 = [0 0];
maxU = 1.5;
maxOm = 2;
maxLV = 4;
N = 12;
z = zeros(N*N*N,2); s = zeros(N*N,4);

delta = linspace (-maxU,maxU,N); 
omega = linspace (-maxOm,maxOm,N);
lat_v = linspace (-maxLV,maxLV,N);
[Delta, Omega, Lat_V] = meshgrid(delta, omega, lat_v);
Delta = reshape(Delta,1,[]); 
Omega = reshape(Omega,1,[]);
Lat_V = reshape(Lat_V,1,[]);

for i = 1:length(Delta)
    z(i,:) = simulate([Lat_V(i) Omega(i)], Delta(i), plant)';
end

save(['model_testing/' basename 'testbed_' datestr(now,'yymmddHHMM')], ...
    'Delta','Omega','Lat_V','z')