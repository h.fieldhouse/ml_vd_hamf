%% Code

% 1. Initialization
settings_vehicle;                      % load scenario-specific settings
basenameGP = 'data/dynmodels/no_mf_dynmodel_SPGP_';

load('data/model_testing/vehicle_testbed_2205311017.mat')
N = length(Delta);

sparsity_range = [10 50 100 200 300 500 700 1000 2000]; %
nIter = size(sparsity_range,2);

sse = nan(nIter,1);
uncert = nan(nIter,1);

for iter = 1:nIter
    disp(['Iteration: ' num2str(iter) ' of ' num2str(nIter)])
    
    nInduce = sparsity_range(iter);
    fileID = [num2str(nInduce) 'of3000ideal_gauss_multivar_2205311034'];
    load([basenameGP fileID '.mat'],'dynmodel')
    
    zGP = zeros(N,2); sGP = zeros(N,4);
    
    for i = 1:length(Delta)
        [M, S] = dynmodel.fcn(dynmodel, [Lat_V(i) Omega(i) 0 0 Delta(i)]', zeros(5));
        zGP(i,:) = M(1:2)'; sGP(i,:) = diag(S);
    end
    
    error = zGP - z;
    sse(iter) = sum(error.^2,'all');
    uncert(iter) = sum(sGP,'all');
    
end