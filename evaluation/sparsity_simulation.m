%% sparsity_simulation.m
% *Summary:* Script to run a dynamics simulation
%
%
%
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Initialization
% # Train GP model on data
% # Run GP model and dynamics simulations


%% Code

% 1. Initialization
train_model = false;

if train_model
    clear all; close all;
    train_model = true;
    settings_vehicle;                      % load scenario-specific settings
    basename = 'data/vehicle_';            % filename used for saving data
    basenameGP = 'data/dynmodel_';
    
    
    fileID = 'ideal_gauss_multivar_2111012111';
    load([basename fileID '.mat'],'dynmodel')
    
    nInduce = 300;                         % adjust sparsity here
    dynmodel.induce = zeros(nInduce,0,1);  % shared inducing inputs (sparse GP)
    
    x = dynmodel.inputs; y = dynmodel.targets;
    
    % 2. Train GP model on data
    trainDynModel;
end

% 3. Run GP model and dynamics simulations
Htest = 100;
% tvec = linspace(0,5,n);
delta = 0.1 * ones(Htest);                           % set necessary constants
z0 = zeros(1,2);
s0 = [0.1 0.1 0]; %.^4;
zGP = zeros(Htest+1,2); zDyn = zeros(Htest+1,2); sGP = zeros(Htest+1,2);
zGP(1,:) = z0; zDyn(1,:) = z0; sGP(1,:) = s0(1:2);
sgp=diag(s0);

for i = 1:Htest                          % simulate in loop
   % [M, S] = dynmodel.fcn(dynmodel, [zGP(i,:) delta(i)]', sgp);
   [Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp] = gp1d(dynmodel, [zGP(i,:) delta(i)]', sgp); 
   % convert all gp outputs from deltax to x:
   
   % Mgpx
   zGP(i+1,:) = Mgp' + zGP(i,:);  % mgp is preceding x, therefore add Mgp (deltax) to get next x and call it Mgpx [2x1] (so that Mgp is now not a difi state)
   
   % Sgpx
   sVgp=sgp*Vgp;  % [3,2] = [3,3] * [3,2]      multiply each column of V by s - correct
   sVTgp=sVgp';   % [2,3]
   Sgpx=sgp(1:2,1:2)+sVgp(1:2,1:2)+sVTgp(1:2,1:2)+Sgp; % [2,2] take just the v and omega states

   
   zDyn(i+1,:) = simulate(zDyn(i,:), delta(i), plant);
   sgp(1:2,1:2) = Sgpx; 
   sGP(i+1,:) = diag(sgp(1:2,1:2));
end

[zDyn zGP sGP]
if train_model
    save([basenameGP 'SPGP_' num2str(nInduce) 'of' num2str(size(x,1)) fileID],'dynmodel')
end