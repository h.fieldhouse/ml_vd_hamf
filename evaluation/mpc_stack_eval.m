stack_l = length(filenames);

% Evaluate tests
rmse_test = zeros(stack_l, ITER);
rmse_opt_y = zeros(stack_l, ITER);

%file_loc='data/results/';
load('data/targets/moose_target_2.mat')
load( [file_loc filenames{1}] )
u=18;

pos = zeros(2,length(t));
for i = 2:length(t)
    pos(:,i) = pos(:,i-1) + u * (t(i)-t(i-1)) * [cos(target(2, i)); sin(target(2, i))];
end

for i = 1:stack_l
    load( [file_loc filenames{i}] )
    for j = 1:ITER
        result = results{j};
        posT = zeros(2,length(t));
        for k = 2:length(t)
            posT(:,k) = posT(:,k-1) + u * (t(k)-t(k-1)) * [cos(result.z(4, k)); sin(result.z(4, k))];
        end
        pos_delta = posT - pos;
        dist_delta2 = sum(pos_delta.^2, 1);
        
        iEND = result.iGP(1,1,end) + STEP;
        rmse_test(i,j) = sqrt(mean(dist_delta2(1:iEND)));
        
        y_delta = result.z(3,:) - target(1, :);
        rmse_opt_y(i,j) = sqrt(mean(y_delta(1:iEND).^2));
    end
end

% Plot results
