%% prediction_rollout.m
% *Summary:* Script to run a dynamics simulation
%
%
% change to function
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Initialization
% # Train GP model on data
% # Run GP model and dynamics simulations


%% Code

% 1. Initialisation
H = length(delta);
% tvec = linspace(0,5,n);
% delta = 0.1 * ones(Htest,1);                           % set necessary constants
% z0 = zeros(1,4);
% s0 = [0.1 0.1 0.1 0.1 0].^4;

zGP = zeros(H+1,4); zDyn = zeros(H+1,4); sGP = zeros(H+1,4);
zGP(1,:) = z0; zDyn(1,:) = z0; sGP(1,:) = s0(1:4);
sgp=diag(s0);

% 2. Run GP model and dynamics simulations
for i = 1:H                          % simulate in loop
   % [M, S] = dynmodel.fcn(dynmodel, [zGP(i,:) delta(i)]', sgp);
   [Mgp, Sgp] = dynmodel.fcn(dynmodel, [zGP(i,:) delta(i)]', sgp); 

   zGP(i+1,:) = Mgp';
   zDyn(i+1,:) = simulate(zDyn(i,:), delta(i), plant);
   sgp(1:4,1:4) = Sgp; 
   sGP(i+1,:) = diag(sgp(1:4,1:4));
end

[zDyn zGP sGP];