%% predict_path.m.m
% *Summary:* Implements the GP to predict vehicle path and variance.
%
%    function M, S = predict_path(dynmodel, delta, z0, s0)
%
%
% *Input arguments:*
%
%   dynmodel  : GP model structure
%   delta     : steer angle delta                                  [H x 1]
%   z0        : initial state                                      [D x 1]
%   s0        : initial variance                                   [D x 1]
%
% *Output arguments:*
%
%   M         : mean predicted states at subsequent timesteps      [H x D]
%   S         : variance of predicted states                       [H x D]
%
%
% Note: It is assumed that the state variables are of the following order:
% D 1.  v:        [m/s]     sideslip velocity
%   2.  omega:    [rad/s]   yaw velocity
%   3.  y:        [m]       lateral displacement
%   4.  theta:    [rad]     yaw angle
%
% U 1.  delta:    [rad]     steering angle
% 
% 
% *Example Usage:*
% 
% settings_vehicle
% load('C:\Users\harry\Documents\MATLAB\pilcoV0.9\scenarios\hamf-vehicle-v2\data\dynmodel_lin_hf.mat')
% H = 30;
% t = linspace(0, H*dt-dt, H)';
% delta = sin(t*2*pi/3);
% z0 = [0 0 0 0];
% s0 = [0.1 0.1 0.1 0.1].^3;
% [M,S] = predict_path(dynmodel,delta,z0,s0);
%
% Last modified by HAMF 2022-01-02

function [zGP, sGP] = predict_path(dynmodel, delta, z0, s0)
%% Code


% 1. Set necessary constants
Htest = length(delta);

zGP = zeros(Htest+1,4); sGP = zeros(Htest+1,25); % ,4);
zGP(1,:) = z0; % sGP(1,:) = s0(1:4);
sgp=diag([s0 0]);
sGP(1,:) = sgp(:)';

% 2. Run GP model
for i = 1:Htest                          % simulate in loop
   [Mgp, Sgp] = dynmodel.fcn(dynmodel, [zGP(i,:) delta(i)]', sgp); 

   zGP(i+1,:) = Mgp';
   sgp(1:4,1:4) = Sgp; 
   sGP(i+1,:) = sgp(:)'; % diag(sgp(1:4,1:4));
end

zGP = zGP(2:end,:);
sGP = sGP(2:end,:);
end
