D = 2;

timeit(@() loop(@(m,s) f1(dynmodel, m, s), 1))
timeit(@() loop(@(m,s) f2(dynmodel, m, s), 1))

function x = loop(f, n)
for i = 1:n
    x0 = randn(3,1);
    s0 = randn(3);
    x = f(x0, s0);
end
end

function [Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp] = f1(dynmodel, m, s)
[Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp]=gp1d_m(dynmodel,m,s);
end

function [Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp] = f2(dynmodel, m, s)
[Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp]=gp1d_m_o(dynmodel,m,s);
end
