%% Code

% 1. Initialization
load('data/model_testing/vehicle_testbed_v1.mat')
N = length(Delta);

load('data\model_testing\sparsity_comparison_v2.mat')
nIter = size(sparsity_range,2);
basenameGP = 'data/dynmodels/dynmodel_SPGP_';

sparsity_timings = nan(nIter, 2);

% rand_in = rand(3,1)-0.5;
% gp_dyn_in = rand_in.*[4; 4; 1];
% disp(gp_dyn_in)

for iter = 1:nIter
    disp(['Iteration: ' num2str(iter) ' of ' num2str(nIter)])
    
    nInduce = sparsity_range(iter);
    fileID = [num2str(nInduce) 'of3000ideal_gauss_multivar_2111220930'];
    load([basenameGP fileID '.mat'],'dynmodel')
    
    f = @() eval_gp(dynmodel, N, Lat_V, Omega, Delta);
    
    sparsity_timings(iter,:) = [sparsity_range(iter) ...
        timeit(f)];
end

function eval_gp(dynmodel, N, Lat_V, Omega, Delta)
zGP = zeros(N,2); sGP = zeros(N,2);

for i = 1:N
    [M, S] = dynmodel.fcn(dynmodel, [Lat_V(i) Omega(i) Delta(i)]', zeros(3));
    zGP(i,:) = M' + [Lat_V(i),Omega(i)]; sGP(i,:) = diag(S);
end

end