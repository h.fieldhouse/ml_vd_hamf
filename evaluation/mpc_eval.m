stack_names = cell(7,2);

rmse_pos = zeros(10,6,7,2);
rmse_y = zeros(10,6,7,2);

%%
stack_names(:,1) = {'mpc_operation_stack_t10-wo-mf-b-3.mat'
    'mpc_operation_stack_t9-wo-mf-b-1.mat'
    'mpc_operation_stack_t11-wo-mf-b-05.mat'
    'mpc_operation_stack_t8-wo-mf-b0.mat'
    'mpc_operation_stack_t12-wo-mf-b05.mat'
    'mpc_operation_stack_t1-wo-mfunc.mat'
    'mpc_operation_stack_t7-wo-mf-b3.mat'};

stack_names(:,2) = {'mpc_operation_stack_t5-w-mf-b-3.mat'
    'mpc_operation_stack_t4-w-mf-b-1.mat'
    'mpc_operation_stack_t14-w-mf-b-05.mat'
    'mpc_operation_stack_t3-w-mf-b0.mat'
    'mpc_operation_stack_t13-w-mf-b05.mat'
    'mpc_operation_stack_t2-w-mfunc.mat'
    'mpc_operation_stack_t6-w-mf-b3.mat'};

ITER = 6;
u = 18;
file_loc = 'data/results/store/';

%%

for i1 = 1:7
    for i2 = 1:2
        load(['data/results/filenames/' stack_names{i1,i2}])
        mpc_stack_eval
        rmse_pos(:,:,i1,i2) = rmse_test;
        rmse_y(:,:,i1,i2) = rmse_opt_y;
    end
end
