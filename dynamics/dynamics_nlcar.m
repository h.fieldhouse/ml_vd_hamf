%% dynamics_nlcar.m
% *Summary:* Implements the ODE for simulating the nonlinear car dynamics.
%
%    function dz = dynamics_nlcar(t, z, delta)
%
%
% *Input arguments:*
%
%	t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   delta     : steer angle delta(t)
%
% *Output arguments:*
%
%   dz    :      state derivative wrt time
%       
%
%
% Note: It is assumed that the state variables are of the following order:
%       v:        [m/s]     sideslip velocity
%       omega:    [rad/s]   yaw velocity
%       y:        [m]       lateral displacement
%       theta:    [rad]     yaw angle
%
%
%
% Last modified by DJC: 2021-10-30

function dz = dynamics_nlcar(t,z,delta)
%% Code
g=9.81; % [N/kg]
a = 1.2;   % [m]     CoM to front axle
b = 1.2;   % [m]     CoM to rear axle
m = 1000;  % [kg]     mass of car
I = 2000;  % [kg m^2] moment of inertia
U = 18;    % [m/s] forward speed
fzf=m*g*b/(a+b);
fzr=m*g-fzf;

% magic formula parameters for lateral force
muyf=1.0;
muyr=1.0;
cyf=0.8;
cyr=0.8;
byf=15;
byr=15;
eyf=-2.0;
eyr=-2.0;

dz = zeros(4,1);

% front slip angle
if isa(delta,'function_handle')
    alphaf=(z(1)+a*z(2))/U - delta(t);
else
    alphaf=(z(1)+a*z(2))/U - delta;
end

% front tyre force
Ff=-fzf*muyf*sin(cyf*atan(byf*alphaf-eyf*(byf*alphaf-atan(byf*alphaf))));
% rear slip angle
alphar=(z(1)-b*z(2))/U;
% rear tyre force
Fr=-fzr*muyr*sin(cyr*atan(byr*alphar-eyr*(byr*alphar-atan(byr*alphar))));

dz(1) = -U*z(2)+(Ff+Fr)/m;   % add cos(z(4))
dz(2) = (Ff*a-Fr*b)/I;
dz(3) = z(1) + U*z(4);  % add cos(z(4)) and sin(z(4)) to remove small angle approximation
dz(4) = z(2);
