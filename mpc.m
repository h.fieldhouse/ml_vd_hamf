%% mpc.m
% *Summary:* Script to run a model predictive control process
%
%
% 
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Initialization
% # Run MPC predict-implement loop

function [z, delta, n_pred, zGP, sGP, iGP, delta_pred, cost_evals] ...
    = mpc(dynmodel, plant, cost, delta, z0, s0, target, TMAX, H, STEP)
%% Code

% 1. Initialisation

z = zeros(4,TMAX+1); z(:,1) = z0;

n_pred = floor((TMAX - H)/STEP);

zGP = zeros(4,H+1,n_pred); sGP = zeros(4,H+1,n_pred);
iGP = zeros(1,H+1,n_pred);
zGP(:,1,1) = z0; sGP(:,1,1) = s0; 
delta_pred = zeros(1,H,n_pred); cost_evals = zeros(1,n_pred);

S0=diag(s0);

options = optimoptions('fmincon','Display','none','SpecifyObjectiveGradient',cost.derivative, ...
    'MaxFunctionEvaluations',cost.mpc_maxfeval,'StepTolerance',cost.mpc_steptol);
problem.options = options;
problem.solver = 'fmincon';
problem.x0 = zeros(1,H);
problem.ub = ones(1,H).*plant.maxU;
problem.lb = - problem.ub;

OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times

% 2. Run MPC predict-implement loop
for p = 1:n_pred
    
    I0 = (p - 1) * STEP + 1;
    problem.x0 = delta(:,I0:I0+H-1);
    
    % Minimize cost
    problem.objective = @(uvec)cost.fcn(uvec, dynmodel, z(:,I0), S0, target(:,I0:I0+H-1), cost);
    [delta(:,I0:I0+H-1), cost_evals(p), exitflag, opt_o] = fmincon(problem);
    disp([num2str(p) '   Exit-flag: ' num2str(exitflag) ', Iter: ' num2str(opt_o.iterations) ', F-eval: ' ...
        num2str(opt_o.funcCount) ', Obj step: ' num2str(opt_o.stepsize) ', 1st opt: ' ... 
        num2str(opt_o.firstorderopt) ', Fval: ' num2str(cost_evals(p))])
    
    delta_pred(1,:,p) = delta(:,I0:I0+H-1);
    
    % Prediction loop
    zGP(:,1,p) = z(:,I0); sgp = diag([s0; 0]); iGP(1,:,p) = I0 + (0:H);
    for i = 1:H
        [Mgp, Sgp] = dynmodel.fcn(dynmodel, [zGP(:,i,p); delta_pred(1,i,p)], sgp); 
        zGP(:,i+1,p) = Mgp; sgp(1:4,1:4) = Sgp; sGP(:,i+1,p) = diag(sgp(1:4,1:4));
    end
    
    % Simulation loop
    for i = I0:I0+STEP-1
        [~,next]=ode45(@(tode,z) plant.dynamics(tode,z,delta(i)),tsim,z(:,i)',OPTIONS);
        z(:,i+1) = next(3,:)';
    end
end
end