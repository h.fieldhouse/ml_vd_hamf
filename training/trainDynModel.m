function dynmodel = trainDynModel(dynmodel, x, y)
% Function for training the dynamics model

dyni = dynmodel.dyni; dyno = dynmodel.dyno; difi = dynmodel.difi; Du = 1;

dynmodel.inputs = [x(:,dyni) x(:,end-Du+1:end)];     % use dyni and ctrl

dynmodel.targets = y(:,dyno);

if isfield(dynmodel, 'mean_func')
    disp('Apply mean function')
    dynmodel.targets = dynmodel.targets - dynmodel.mean_func(dynmodel, dynmodel.inputs')';
else
    dynmodel.targets(:,difi) = dynmodel.targets(:,difi) - x(:,dyno(difi));
end
    
dynmodel = dynmodel.train(dynmodel);  %  train dynamics GP
end