options = optimoptions('fmincon','Display','iter','SpecifyObjectiveGradient',false, ...
    'MaxFunctionEvaluations',3000,'StepTolerance',1e-10);
problem.options = options;
problem.solver = 'fmincon';
problem.x0 = [-5 -4 100 0.9 -0.7 0 0 0 0.9 0 0.2];
% Minimize cost
problem.objective = @(x)sig_error(x, dynmodel.inputs', dynmodel.targets');
[x, cost_eval, exitflag, opt_o] = fmincon(problem);

function [error, dedp] = sig_error(x, inputs, targets)
dynmodel.s_coeff = x(1:3);
dynmodel.m_coeff = reshape(x(4:11),4,2);
dynmodel.m_coeff = dynmodel.m_coeff';
error = sum((meanfunc_sig(dynmodel, inputs) - targets).^2,'all')+x(6)^2+x(10)^2;
dedp = 0;
end