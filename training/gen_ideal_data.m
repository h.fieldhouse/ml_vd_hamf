%% gen_ideal_data.m
% *Summary:* Script to run a dynamics simulation to generate training data
%
%
%
%
% Last modified by HAMF: 2021-11-01
%
%% High-Level Steps
% # Load parameters
% # Generate training input
% # Create initial training data using dynamics simulation

%% Code

% 1. Initialization
clear all; close all;
settings_vehicle;                % load scenario-specific settings
basename = 'vehicle_';           % filename used for saving data

Hinitial = 3000;
x=[];y=[];
tsim=[0 dt/2 dt]; % specify >2 times otherwise ode45 returns all intermediate integration points.
tvecinitial=(0:1:Hinitial-1)*dt; % time array for initial training input
ngp=2;

% 2. Generate training input
maxU=0.5;
distX = @normrnd; distU = @trnd;
stdX1=1.0;stdX2=1.0;stdU=0.3;
xvec=[distX(0,1,Hinitial,2).*[stdX1 stdX2] zeros(Hinitial,2)];
uvec=distU(1,Hinitial,1)*stdU; % generate training input - student t
outside_i = [find(uvec<-maxU); find(uvec>maxU)];
while ~isempty(outside_i)
    uvec(outside_i)=stdU*trnd(1,size(outside_i,1),1);
    outside_i = [find(uvec<-maxU); find(uvec>maxU)];
end

% 3. Initial data aquisition

for k=1:Hinitial % run the plant with uvec and store response
    [tode,next]=ode45(@(tode,z) plant.dynamics(tode,z,uvec(k,1)),tsim,xvec(k,:));
    yy(k,:)=(next(3,1:2)+ (chol(plant.noise(1:2,1:2))*randn(ngp,1))'); % training data for gp. This is the required output at k+1.
end
x = [x; [xvec(1:Hinitial,1:2) uvec(1:Hinitial,1)]]; % input data for training gp
y = [y; yy(1:Hinitial,1:2)];       % output data for training gp


dynmodel.inputs = x; dynmodel.targets = y;

generation = struct('dynamics',plant.dynamics,'maxU',maxU,'stdU',stdU,'stdX1',stdX1,'stdX2',stdX2,'H',Hinitial,'J',1,...
    'distX',distX,'distU',distU,'nu',1);

save(['data/training_data/' basename 'ideal_gauss_multivar_' datestr(now,'yymmddHHMM')],'dynmodel','generation')
disp(['data/training_data/' basename 'ideal_gauss_multivar_' datestr(now,'yymmddHHMM')])

if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
ax1 = axes;
scatter3(ax1,dynmodel.inputs(:,3),dynmodel.inputs(:,2),dynmodel.targets(:,2), 60, dynmodel.inputs(:,1),'filled')
xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
cb1 = colorbar(ax1);
cb1.Label.String = 'Sideslip vel. [k]';
cb1.Label.FontSize = 14;
title(ax1,'Scatter plot of collected training datapoints')