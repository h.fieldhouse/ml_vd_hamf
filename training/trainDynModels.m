% 1. Initialization
basename = 'data/vehicle_';            % filename used for saving data
basenameGP = 'data/covfunc_dynmodel_';

fileID = 'ideal_gauss_multivar_2111021009';

set_covfunc = true;

sparsity_range = [10 50 100 200 300];
nIter = size(sparsity_range,2);
training_time = nan(nIter,2);

for iter = 1:nIter
    
    load([basename fileID '.mat'],'dynmodel')
    
    nInduce = sparsity_range(iter);
    disp(nInduce)
    dynmodel.induce = zeros(nInduce,0,1);  % shared inducing inputs (sparse GP)
    
    if set_covfunc
        dynmodel.covfunc = {'covSum', {'covSEard', 'covSEard', 'covNoise'}};   % specify double ARD covariance
        
        D = size(dynmodel.inputs,2); E = size(dynmodel.targets,2);   % get variable sizes
        
        dynmodel.hyp = rand(2*D+3,E); 
        %lh = repmat([log(std(gpmodel.inputs)) 0 -1]',1,E);   % init hyp length scales
        dynmodel.hyp(D+1,:) = log(std(dynmodel.targets));                      %  signal std dev
        dynmodel.hyp(2*D+2,:) = log(std(dynmodel.targets));                      %  signal std dev
        dynmodel.hyp(2*D+3,:) = log(std(dynmodel.targets)/10);                     % noise std dev
    end
    
    x = dynmodel.inputs; y = dynmodel.targets;
    
    % 2. Train GP model on data
    training_time(iter,1) = nInduce;
    tic
    trainDynModel;
    training_time(iter,2) = toc;
    
    % 3. Save GP model
    save([basenameGP 'SPGP_' num2str(nInduce) 'of' num2str(size(x,1)) fileID],'dynmodel')
end

save('model_testing/sparsity_training_timings_v2', 'training_time')