%% gen_training_data.m
% *Summary:* Script to run a dynamics simulation to generate training data
%
%
%
%
% Last modified by HAMF: 2021-10-30
%
%% High-Level Steps
% # Load parameters
% # Generate training input
% # Create J initial trajectories by applying random controls

%% Code

% 1. Initialization
clear all; close all;
settings_vehicle;                % load scenario-specific settings
basename = 'vehicle_';           % filename used for saving data

Hinitial = H;
x=[];y=[];
tsim=[0 dt/2 dt]; % specify >2 times otherwise ode45 returns all intermediate integration points.
tvecinitial=(0:1:Hinitial-1)*dt; % time array for initial training input
ngp=2;

% 2. Generate training input
rmpath('../../base')
maxU=plant.maxU;
omegainitial=0.5; % freq sinusoidal component of training input
stdUa=0.3; % amplitude of sinusoidal component of training input
stdUs=0.3; % std dev of initial random component of training input
phase = 2 * pi * rand(1,1,J);
uvec=stdUa*sin(omegainitial*tvecinitial'+phase)+stdUs*trnd(1,Hinitial,1,J); % generate training input - sinusoid + white noise
uvec=min(uvec,maxU);uvec=max(uvec,-maxU);
addpath('../../base')

% 3. Initial J random rollouts
for j=1:J
    xvec(:,1)=[0 0]'; % initial state of nonlinear plant [v; omega]
    for k=1:Hinitial % run the plant with uvec and store response
        [tode,next]=ode45(@(tode,z) plant.dynamics(tode,z,uvec(k,1,j)),tsim,xvec(:,k)');
        xvec(:,k+1)=next(3,:)';  % ode45 returns output at all times in tsim.
        yy(k,:)=(xvec(:,k+1)+ chol(plant.noise)*randn(ngp,1))'; % training data for gp. This is the required output at k+1.
    end
    x = [x; [xvec(1:2,1:Hinitial)' uvec(1:Hinitial,1,j)]]; % input data for training gp
    y = [y; yy(1:Hinitial,1:2)];       % output data for training gp
end

dynmodel.inputs = x; dynmodel.targets = y;

generation = struct('omega',omegainitial,'maxU',maxU,'stdUa',stdUa,'stdUs',stdUs,'H',Hinitial,'J',J,'dist',@trnd,'nu',1);

filename = ['data/' basename 'nl_sin_rand_in_' datestr(now,'yymmddHHMM')];
save(filename,'dynmodel','generation')
disp(filename)

if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
ax1 = axes;
scatter3(ax1,dynmodel.inputs(:,3),dynmodel.inputs(:,2),dynmodel.targets(:,2)+dynmodel.inputs(:,2), 60, dynmodel.inputs(:,1),'filled')
xlabel(ax1,'delta (rad)'); ylabel(ax1,'omega[k] (rad/s)'); zlabel(ax1,'omega[k+1] (rad/s)')
cb1 = colorbar(ax1);
cb1.Label.String = 'Sideslip vel. [k]';
cb1.Label.FontSize = 14;
title(ax1,'Scatter plot of collected training datapoints')