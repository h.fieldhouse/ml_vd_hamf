clear all; close all;
settings_vehicle
load('data\targets\moose_target_2.mat')
use_existing_dynmodel = false;
TMAX = 163; H = 20; STEP = 10; ITER = 5;
dynmodel.induce = zeros(140,0,1);
t = linspace(0, TMAX*dt-dt, TMAX+1);
delta_i = zeros(1,TMAX);
for i = 1:length(delta_i)
    delta_i(i) = 2*(target(2,i+1)-target(2,i));
end
z0 = [0 0 0 0]'; ngp = 4;

test_vals = [10 30 50 100];

results = cell(length(test_vals),1);

for i = 1:length(test_vals)
    
    x = test_vals(i);
    
    %   W       Weight matrix               `       [ D x D ]
    % (1,1) sideslip vel v;     (2,2) yaw vel omega;     (3,3) lateral displacement y
    % (4,4) heading angle psi;  (5,5) steering angle u
    Wstar=zeros(4);
    Wstar(1,1)=1;      % penalise y path following error
    Wstar(2,2)=15;     % penalise psi path following error
    Wstar(3,3)=5;      % penalise steering angle
    Wstar(4,4)=x;      % penalise alpha_f front slip angle
    
    C=[ 0 0 1 0 0           % transform state vector to outputs of interest
        0 0 0 1 0
        0 0 0 0 1
        1/U a/U 0 0 -1 ];
    
    cost.W=C'*Wstar*C;
    
    [z,delta_f,n_pred,zGP,~,iGP,delta_pred,cost_evals]=mpc_dyn(0,plant,cost,delta_i,z0,0,target,TMAX,H,STEP);
    
    result.z = z; result.delta_f = delta_f; result.n_pred = n_pred;
    result.zGP = zGP; result.iGP = iGP;
    result.delta_pred = delta_pred; result.cost_evals = cost_evals;
    result.cost = cost;
    results{i} = result;

end