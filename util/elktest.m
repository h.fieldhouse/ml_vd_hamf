% define target path for vehicle speed of 18m/s
dt=0.05;
lanechange=4.0; % lateral displacement / m  ISO is 4m
tfinal=8.2; % total time duration / s
nfinal=nearest(tfinal/dt);
tsection=1; % time duration of half-cos / s
nsection=tsection/dt;
t=0:dt:tfinal-dt;
ytarget=zeros(nfinal,1);
ytarget(nsection*2:nsection*3)=lanechange*(0.5-0.5*cos(pi*(0:1:nsection)/nsection));
ytarget(nsection*3:nsection*3+nsection/5)=lanechange*ones(size(ytarget(nsection*3:nsection*3+nsection/5)));
ytarget(nsection*3+nsection/5:nsection*4+nsection/5)=lanechange*(0.5+0.5*cos(pi*(0:1:nsection)/nsection));
figure(1) % plot the target path
plot(t,ytarget,'kx-')
title('lateral displ / m')
xlabel('time / s')
