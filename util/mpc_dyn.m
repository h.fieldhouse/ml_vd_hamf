%% mpc.m
% *Summary:* Script to run a model predictive control process
%
%
% 
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Initialization
% # Run MPC predict-implement loop

function [z, delta, n_pred, zGP, sGP, iGP, delta_pred, cost_evals] ...
    = mpc_dyn(~, plant, cost, delta, z0, ~, target, TMAX, H, STEP)
%% Code

% 1. Initialisation

z = zeros(4,TMAX+1); z(:,1) = z0;

n_pred = floor((TMAX - H)/STEP);

zGP = zeros(4,H+1,n_pred); zGP(:,1,1) = z0; 
iGP = zeros(1,H+1,n_pred);
delta_pred = zeros(1,H,n_pred); cost_evals = zeros(1,n_pred);

options = optimoptions('fmincon','Display','iter','SpecifyObjectiveGradient',cost.derivative, ...
    'MaxFunctionEvaluations',500,'StepTolerance',1e-6);
problem.options = options;
problem.solver = 'fmincon';
problem.x0 = zeros(1,H);
problem.ub = ones(1,H).*plant.maxU;
problem.lb = - problem.ub;

OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times

% 2. Run MPC predict-implement loop
for p = 1:n_pred
    disp(p)
    
    I0 = (p - 1) * STEP + 1;
    problem.x0 = delta(:,I0:I0+H-1);
    
    % Minimize cost
    problem.objective = @(uvec)cost_path_dynamics(uvec, plant, z(:,I0), 0, target(:,I0:I0+H-1), cost);
    [delta(:,I0:I0+H-1), cost_evals(p)] = fmincon(problem);
    
    delta_pred(1,:,p) = delta(:,I0:I0+H-1);
    
    % Simulation loop
    for i = I0:I0+STEP-1
        [~,next]=ode45(@(tode,z) plant.dynamics(tode,z,delta(i)),tsim,z(:,i)',OPTIONS);
        z(:,i+1) = next(3,:)';
    end
end
sGP = nan; % for compatability only

end