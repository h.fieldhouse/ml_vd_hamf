%% sine_var_freq.m
% *Summary:* Function to generate a sine wave with a varying frequency
%
%    function y = sine_var_freq(dt, f, phi)
%
%
% *Input arguments:*
% 
%   dt      timestep    (sec)
%   f       frequencies (Hz)                         [ n x 1 ]
%   phi     (optional) starting phase (rad)
%
%
% *Output arguments:*
%
%   y       sine wave
% 
%
% 
%
% Last modified by HAMF: 2021-10-31
%
%% High-Level Steps
% # 

%% Code
function y = sine_var_freq(dt, f, phi)

y = sin(2*pi*cumsum(f)*dt+phi);

end