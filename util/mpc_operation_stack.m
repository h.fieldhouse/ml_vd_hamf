clear all; close all;
settings_vehicle
load('data\targets\moose_target_2.mat')
use_existing_dynmodel = false;
TMAX = 163; H = 20; STEP = 10; ITER = 6;
t = linspace(0, TMAX*dt-dt, TMAX+1);
delta_i = zeros(1,TMAX);
for i = 1:length(delta_i)
    delta_i(i) = 2*(target(2,i+1)-target(2,i));
end
z0 = [0 0 0 0]'; ngp = 4;
s0 = [0.1 0.1 0.1 0.1]'.^3;

mpc_preinit = true;

test_name = 'mpc_tb_-3_3_sig_10';

q = ones(1,10); %
s_var = [-3.*q, -1.*q, -0.5.*q, 0.*q, 0.5.*q, 1.*q, 3.*q];

mean_func_default = dynmodel.mean_func;
filenames = cell(length(s_var),1);

for i = 1:length(s_var)
    x = s_var(i); % Set var

%     if ~x
%         dynmodel = rmfield(dynmodel, 'mean_func');
%     end

    cost.b=x;

    disp(dynmodel)
    disp(cost)
    
    % Run MPC
    learn_mpc
    cd data/results
    d = dir('*.mat');
    [~, index] = max([d.datenum]);
    filenames{i} = d(index).name;
    cd ../..
    mpc_reset
end

save(['data/results/mpc_operation_stack_' test_name], 's_var', 'filenames')