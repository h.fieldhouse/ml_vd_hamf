% define target path for vehicle speed of 18m/s
dt=0.05;
U=18; % speed in m/s
lanechange=4.0; % lateral displacement / m  ISO is 4m
tfinal=8.2; % total time duration / s
nfinal=nearest(tfinal/dt);
tsection=1; % time duration of half-cos / s
nsection=tsection/dt;
t=0:dt:tfinal-dt;

ytarget=zeros(nfinal,1);
%%%%% Cartesian target y
% ytarget(nsection*2:nsection*3)=lanechange*(0.5-0.5*cos(pi*(0:1:nsection)/nsection));
% ytarget(nsection*3:nsection*3+nsection/5)=lanechange*ones(size(ytarget(nsection*3:nsection*3+nsection/5)));
% ytarget(nsection*3+nsection/5:nsection*4+nsection/5)=lanechange*(0.5+0.5*cos(pi*(0:1:nsection)/nsection));
% figure(1) % plot the target path
% plot(t,ytarget,'kx-')
% title('lateral displ / m')
% xlabel('time / s')

%%%%% Intrinsic target theta (theta is the angle of the target path)
% Note that ytarget here is calculated by integrating the thetas and
% multiplying by U*dt. ytarget is not the same as y in cartesian coordinates when angles are large,
% but nevertheless can be used to evaluate the lateral path following
% error if the corresponding y of the vehicle is calculated in a similar
% way.
maxkappa=0.1;
kappa=zeros(nfinal,1);
kappa(nsection*2:nsection*3)=maxkappa*sawtooth(2*pi*(0:1:nsection)/nsection+pi/2,0.5);
kappa(nsection*3+nsection/5:nsection*4+nsection/5)=-maxkappa*sawtooth(2*pi*(0:1:nsection)/nsection+pi/2,0.5);
ytarget(1)=0;
for k=1:nfinal-1
    if k==1
        thetatarget(k)=U*dt*kappa(k);
    else
        thetatarget(k)=thetatarget(k-1)+U*dt*kappa(k);
    end
    ytarget(k+1)=ytarget(k)+U*dt*thetatarget(k);  % add sin to remove small angle approximation
end
thetatarget(nfinal)=thetatarget(k);
figure(1) % plot the target path
subplot(211)
quiver(U*t,ytarget',cos(thetatarget),sin(thetatarget),0.05)
title('target y / m')
xlabel('distance x / m')
subplot(212)
plot(U*t,thetatarget,'kx-')
title('target theta / rad')
xlabel('time / s')

target=[ytarget'; thetatarget]; % [2 x nfinal]
