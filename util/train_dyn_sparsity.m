settings_vehicle

% 1. Initialization
basename = 'data/training_data/vehicle_';            % filename used for saving data
basenameGP = 'data/dynmodels/no_mf_dynmodel_';

fileID = 'ideal_gauss_multivar_2205311034';

sparsity_range = [10 50 100 200 300 500 700 1000 2000]; %
nIter = size(sparsity_range,2);
training_time = nan(nIter,2);

for iter = 1:nIter
    
    load([basename fileID '.mat'],'dynmodel')
    
    dynmodel.fcn = @gp1d_lin_hf;         % function for GP predictions
    %dynmodel.mean_func = @meanfunc_sig;  % function for adjusting pre-GP mean
    if isfield(dynmodel, 'mean_func')
        dynmodel = rmfield(dynmodel, 'mean_func');
    end
    dynmodel.train = @train;             % function to train dynamics model
    dynmodel.induce = zeros(10,0,1);     % shared inducing inputs (sparse GP)
    % adjust sparsity here
    dynmodel.dyni = dyni;
    dynmodel.dyno = dyno;
    dynmodel.difi = difi;
    
    dynmodel.m_coeff = [ 0.9 -0.73 0, 0.33
        0.04 0.92 0.01 0.27 ];
    
    dynmodel.s_coeff = [ -1.36 -1.13 25 ];
    
    dynmodel.Alin=[ 0 0 0 0
        0 0 0 0
        dt 0 1 U*dt
        0 dt 0 1 ];
    
    dynmodel.Blin=[ 0
        0
        0
        0 ];
    
    dynmodel.Bd = [ 1 0
        0 1
        0 0
        0 0 ];
    
    dynmodel.trainOpt = [300 500];       % defines the max. number of line searches
    % when training the GP dynamics models
    % trainOpt(1): full GP,
    % trainOpt(2): sparse GP (FITC)
    
    nInduce = sparsity_range(iter);
    disp(nInduce)
    dynmodel.induce = zeros(nInduce,0,1);  % shared inducing inputs (sparse GP)
    
    x = dynmodel.inputs; y = dynmodel.targets;
    
    % 2. Train GP model on data
    training_time(iter,1) = nInduce;
    tic
    dynmodel = trainDynModel(dynmodel, x, y);
    training_time(iter,2) = toc;
    
    % 3. Save GP model
    save([basenameGP 'SPGP_' num2str(nInduce) 'of' num2str(size(x,1)) fileID],'dynmodel')
end

save('model_testing/sparsity_training_timings_v2', 'training_time')