function [x, y] = random_rollout(x0, Length, plant)

ngp = size(x0,1);
z = zeros(size(x0,1),Length+1);
delta = normrnd(0,1,1,Length);
maxU = plant.maxU;
outside_i = [find(delta<-maxU) find(delta>maxU)];
while ~isempty(outside_i)
    delta(outside_i)=normrnd(0,1,1,size(outside_i,2));
    outside_i = [find(delta<-maxU) find(delta>maxU)];
end
OPTIONS = odeset('RelTol', 1e-12, 'AbsTol', 1e-12);     % accuracy of ode45
tsim=[0 plant.dt/2 plant.dt];                           % ode45 output times



for i = 1:Length
    [~,next]=ode45(@(tode,z) plant.dynamics(tode,z,delta(i)),tsim,z(:,i)',OPTIONS);
    z(:,i+1) = next(3,:)';
end

x = [z(:,1:end-1)' delta(1,:)'];
y = (z(:,2:end) + chol(plant.noise)*randn(ngp,1))';

end