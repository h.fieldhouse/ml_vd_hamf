clear dynmodel.inputs dynmodel.targets

x = []; y = [];
dynmodel.induce = zeros(size(dynmodel.induce,1),0,1);    % shared inducing inputs (sparse GP)
                                     
if exist('mean_func_default','var')
    dynmodel.mean_func = mean_func_default;
end
