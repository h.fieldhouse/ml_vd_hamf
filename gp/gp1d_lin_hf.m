function [M, S] = gp1d_lin_hf(dynmodel, m, s)
% 2021-11-10 DJ Cole (djc13)
% Implements the certain+uncertain dynamics in Hewing et al 2018, arXiv:1711.06586v2
% Assumes gp trained on differences (difi)
% *Input arguments:*
%   dynmodel   gp model of v and omega states, with u input.
%   m          mean of the test distribution                         [ D  x  1 ]
%   s          covariance matrix of the test distribution            [ D  x  D ]
%              for m and s, D=5 and states and input are     v, omega, y, psi, u
%   Alin       state matrix of the linear dynamics [E x E]
%   Blin       input matrix of the linear dynamics [E x 1]
%   Bd         couples the output of the GPs into the state vector [E x 2]
% *Output arguments:*
%   M          mean of pred. distribution                            [ E  x  1 ]
%   S          covariance of the pred. distribution                  [ E  x  E ]
%              for M and S, E=4 and states are     v, omega, y, psi

Alin = dynmodel.Alin; Blin = dynmodel.Blin; Bd = dynmodel.Bd;

% set up the inputs to gp1d.m 
mgp=[m(1:2); m(5)]; % mgp is [3x1] (v, omega, u), extract these three signals from the five in m
sgp=[s(1:2,1:2) zeros(2,1); zeros(1,3)]; % [3x3] % Neglects variance in u

% Note: slightly modify the PILCO version of gp1d.m in gp1d_m.m by commenting out the
% 'reshape' functions at the end of gp1d.m
[Mgp, Sgp, Vgp, dMdmgp, dSdmgp, dVdmgp, dMdsgp, dSdsgp, dVdsgp] = gp1d_m(dynmodel, mgp, sgp);

% Take account of difi by converting all gp outputs from deltax to x:

% Mgpx - see Deisenroth's thesis    
% Mgpx - mean function effect - HAMF
if isfield(dynmodel, 'mean_func')
    Mgpx=Mgp + dynmodel.mean_func(dynmodel, mgp);
else
    Mgpx=mgp(1:2)+Mgp;  % mgp is input x, therefore add Mgp (deltax) to get output x and call it Mgpx [2x1] (so that Mgp is now not a difi state)
end

% Sgpx - see Deisenroth's thesis
sVgp=sgp*Vgp;  % [3,2] = [3,3] * [3,2]      multiply each column of V by s  (okay)
sVTgp=sVgp';   % [2,3]                 
Sgpx=sgp(1:2,1:2)+sVgp(1:2,1:2)+sVTgp(1:2,1:2)+Sgp; % [2,2] take just the v and omega states

% dMdmgpx
dMdmgpx=dMdmgp(1:2,1:2)+eye(2); % [2,2]

% The commented lines below are only required if derivatives of M and S are
% to be calculated - requires additional code as well.
% % dMdugpx
% dMdugpx=dMdmgp(1:2,3); % [2,1]
% 
% % dMdsgpx
% dMdsgpx=dMdsgp(:,1:2,1:2);% dMdsgpx [2,2,2], dMdsgp [2,3,3]
% 
% % dSdmgpx
% for i=1:3
%     sdVdmgp(:,:,i)=sgp*dVdmgp(:,:,i); % [3,2,3] = [3,3] * [3,2,3]
% end
% sdVdmgp2=sdVdmgp(1:2,1:2,1:2);
% sdVdmgp2T(:,:,1)=sdVdmgp2(:,:,1)';
% sdVdmgp2T(:,:,2)=sdVdmgp2(:,:,2)';
% dSdmgpx = real(sdVdmgp2 + sdVdmgp2T + dSdmgp(1:2,1:2,1:2)); % sdVdmgp+sdVdmTgp+dSdmgp  removed the u terms
% 
% % dSdugpx
% sdVdugp=sdVdmgp(1:2,1:2,3);
% dSdugpx = real(sdVdugp + sdVdugp' + dSdmgp(1:2,1:2,3)); % [2,2,1]
% 
% % dSdsgpx
% for i=1:3
%     for j=1:3
%         sdVdsgp(:,:,i,j)=sgp*dVdsgp(:,:,i,j); % [3,2,3,3]=[3,3] * [3,2,3,3] 
%     end
% end
% 
% for i=1:2
%     for j=1:2
%         sdVdsgp2(:,:,i,j)=sdVdsgp(1:2,1:2,i,j);
%         sdVdsgp2T(:,:,i,j)=sdVdsgp2(:,:,i,j)';
%     end
% end
% 
% dSdsgpx = real(reshape(eye(4),[2,2,2,2]) + sdVdsgp2 + sdVdsgp2T + dSdsgp(1:2,1:2,1:2,1:2)); 



% Finally, form M and S, see Hewing et al eqn(14).
% means M
M(1:4,1)=Alin*m(1:4,1)+Blin*m(5,1)+Bd*real(Mgpx); % okay
% covariances S
S=Alin*s(1:4,1:4)*Alin' + Alin*s(1:4,1:4)'*[dMdmgpx(1:2,1:2) zeros(2)]'*Bd' + Bd*[dMdmgpx(1:2,1:2) zeros(2)]*s(1:4,1:4)*Alin' + Bd*Sgpx*Bd'; % eqn 14b of Hewing

