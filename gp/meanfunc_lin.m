%% meanfunc_lin.m
%
% *Summary:* implement a fitted linear mean function to the vehicle
% dynamics
%
% Inputs:
%   inputs                  [E*1]
%
%
% dynmodel.m_coeff = [ 0 0.225 -0.712 1.5275 
%                      0 0.006 -0.164 1.2524 ];
%
%
%

function target_mean = meanfunc_lin(m_coeff, inputs)
%% Code

target_mean = m_coeff * [ones(1,size(inputs,2)); inputs];

end
