%% meanfunc_lin.m
%
% *Summary:* implement a fitted linear mean function to the vehicle
% dynamics
%
% Inputs:
%   inputs                  [E*n]
%       v
%       omega
%       delta
%
%
%

function target_mean = meanfunc_sig(dynmodel, inputs)
%% Code

sigmoid = -0.5 + 1./(1 + exp(- dynmodel.s_coeff * inputs));

target_mean = dynmodel.m_coeff * [inputs; sigmoid];

end
